// creating and exporting the function 'v' with argument 'o'
// here 'o' represents the argument for passing object into 'v'
module.exports = function v(o) {
    if (typeof o == 'object') { // checking if the one passed as 'o' is an object or not
        let a = []; // declaring an empty array
        for (const x in o) { // iterating through the object properties
            a.push(o[x]) // pushing the values into new array
        }
        return a // returning the array of values
    }
    return [] // returning empty array if passed is not an object type
}