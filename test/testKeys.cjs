// importing the object from file objects.cjs
const o = require("../objects.cjs")
// importing the 'k' function from keys.cjs
const k = require("../keys.cjs")
// printing the array of keys returned from function 'k'
console.log(k(o));