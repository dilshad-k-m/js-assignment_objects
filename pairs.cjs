// creating and exporting the function 'p' with parameter 'o'
module.exports = function p(o) {
    if (typeof o == 'object') // checking if the value passed into 'o' is an object or not
    {
        let a =[] ; // declaring an empty array
        for (const x in o) { // iterating through the object properties
            a.push([x, o[x]]);// pushing keys and values into array as another array
        }
        return a; // returning the array of key,value pairs
    }
    return []; // returning empty array if argument passed is not an object
}