// creating and exporting the function 'k' with argument 'o'
// here 'o' represents the parameter through which objects are passed into the function
module.exports = function k(o) {
    if (typeof o == 'object') {
        let a =[];// declaring an empty array
        for (const x in o){ // using 'for in' iterating through the object properties
            a.push(x); // adding keys into the array
        }
        return a; // returning the array containing all object keys
    }
    return []; // returning empty array if passed argument is not an object
}